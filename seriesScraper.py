from cricinfoScraper import scrape_match_text
import requests

season_url = "http://core.espnuk.org/v2/sports/cricket/leagues/{}/seasons/1/events"

league_id = 19430;

def get_series_comms(league_id):
    response = requests.get(season_url.format(league_id))
    data = response.json()
    # print(data)
    events = data['items']
    for event in events:
        event_url = event['$ref']
        event_data = requests.get(event_url).json()
        event_id = event_data['id']
        # print(event_id)
        scrape_match_text(league_id,event_id)



get_series_comms(league_id)
