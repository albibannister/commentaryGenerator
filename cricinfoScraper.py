# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 22:27:52 2020

@author: albib
"""

import requests
import sys

game_id_list = [1192873,1192874,]
game_1_2020_id = 1152846
game_1_2018_id = 1072305
league_id_2020 = 18659
league_id_2018 = 10883
URL = "https://hsapi.espncricinfo.com/v1/pages/match/comments?lang=en&leagueId={}&eventId={}&period={}&page={}&filter=full&liveTest=false"
URL2 = "https://hsapi.espncricinfo.com/v1/pages/match/comments?lang=en&leagueId=10883&eventId=1072305&period=4&page=10&filter=full&liveTest=false"
eventId = 1152846


def remove_html_tags(text):
    """Remove html tags from a string"""
    import re
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


# writes commentary from given match to output file
def scrape_match_text(league_id, game_id):
    get_URL = URL.format(league_id, game_id, 1, 1)
    print(get_URL)
    response = requests.get(get_URL)
    data = response.json()
    print(data)
    page_count = data["pagination"]["pageCount"]

    output = open("series{}_game{}".format(league_id,game_id), "w")

    more_days = True
    day = 1

    while more_days:
        print("Day {} has {} pages of commentary".format(day, page_count))
        # gather text from each page of commentary data for a given day
        print("Gathering data for day {}".format(day))
        for page_index in range(1, page_count + 1):
            response = requests.get(URL.format(league_id, game_id, day, page_index))
            try:
                data = response.json()
                commentary = data["comments"]
                if len(commentary) > 0:
                    # print(data)
                    for comment in reversed(commentary):
                        cleaned_text = remove_html_tags(comment["text"])
                        output.write(cleaned_text + "\n")
                else:
                    more_days = False
            except:
                print("error")
                print(data)
        day += 1



    output.close()



# first_match_id = game_1_2018_id
# matches_in_series = 5
# #
# for match_id in range(first_match_id,first_match_id + matches_in_series):
#     scrape_match_text(league_id_2018, match_id)
# scrape_match_text(18659, 1072305)

