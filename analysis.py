import os
from os import path

import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize

# nltk.download('wordnet')
porter = nltk.PorterStemmer()
wnl = nltk.WordNetLemmatizer()


def get_raw(name):
    commentary_path = "{}".format(name)
    files = os.listdir(commentary_path)
    print(files)
    raw_text = ""
    for file in files:
        commentary_text = open(path.join(commentary_path, file)).read()
        raw_text += commentary_text
    return raw_text


def preprocess(raw_text):
    words = word_tokenize(raw_text)
    words = [word for word in words if len(word) > 1]
    words = [word for word in words if not word.isnumeric()]
    words = [word.lower() for word in words]
    words = [word for word in words if word not in stop]
    stemmed = [wnl.lemmatize(word) for word in words]
    text = nltk.Text(words)
    return stemmed


def show_frequency_dist(year):
    commentary_path = "commentary/{}".format(year)
    files = os.listdir(commentary_path)
    print(files)
    raw_text = ""
    for file in files:
        commentary_text = open(path.join(commentary_path, file)).read()
        raw_text += commentary_text

    words = word_tokenize(raw_text)
    words = [word for word in words if len(word) > 1]
    words = [word for word in words if not word.isnumeric()]
    words = [word.lower() for word in words]
    words = [word for word in words if word not in stop]
    stemmed = [wnl.lemmatize(word) for word in words]
    text = nltk.Text(words)

    # print(text)
    fdist = nltk.FreqDist(text)
    stemmed_dist = nltk.FreqDist(stemmed)
    # print(fdist)
    print(fdist.most_common(20))
    print(stemmed_dist.most_common(20))


# stop = stopwords.words("english")
# # print(stop)
#
# # show_frequency_dist(2018)
# # show_frequency_dist(2019)
# commentary =  preprocess(get_raw(2018))
# bigrams = nltk.bigrams(commentary)
# trigrams = nltk.trigrams(commentary)
# pentgrams = nltk.ngrams(commentary,5)
# # for bigram in bigrams:
# #     print(bigram)
#
# bigram_dist = nltk.FreqDist(bigrams)
# trigrams_dist = nltk.FreqDist(trigrams)
# pent_grams_dist = nltk.FreqDist(pentgrams)
# print(bigram_dist.most_common(50))
# print(trigrams_dist.most_common(50))
# print(pent_grams_dist.most_common(50))
